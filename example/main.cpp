#include <imgui.h>
#include <implot.h>
#include <imgui_file_dialog.h>

#include <imgui_impl_vulkan.h>
#include <imgui_impl_glfw.h>

#include <vk/ext/imgui/context.h>
#include <vk/ext/imgui/frame.h>
#include <vk/ext/imgui/render_surface.h>
#include <vk/ext/guard/glfw.h>
#include <vk/ext/guard/tracy_frame.h>
#include <vk/ext/guard/tracy_command_buffer.h>
#include <vk/ext/glfw_surface.h>
#include <vk/ext/trivial/device.h>
#include <vk/ext/trivial/instance.h>

#include <vk/guard/command_buffer.h>

#include <vk/device.h>
#include <vk/instance.h>
#include <vk/physical_device_info.h>

#include <fstream>
#include <iostream>

struct GUI
{
    vk::ext::imgui::ContextGuard context;

    bool show = true;
    std::string path;

    void render(VkCommandBuffer const cmd)
    {
        auto const &[window, imGuiContext, imPlotContext] = context.cref();
        vk::ext::imgui::FrameGuard const guard = {{imGuiContext, imPlotContext, cmd}};

        if(show)
        {
            ImGui::ShowDemoWindow(&show);
            ImGui::ShowMetricsWindow(&show);
            ImPlot::ShowDemoWindow(&show);
        }
        
        ImGuiIO &io = ImGui::GetIO();
        {
            ImGui::SetNextWindowPos(ImVec2(0.f, 0.f));
            ImGui::SetNextWindowSize(ImVec2(500.f, 130.f));
            ImGui::Begin("Проверяем utf-8", nullptr, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize);
            if(ImGui::Button("Выйти"))
                glfwSetWindowShouldClose(context.cref().window, GLFW_TRUE);
            ImGui::Text("%.2f ms, %.1f FPS", 1000.0f / io.Framerate, io.Framerate);

            if(ImGui::Button("Открыть файл"))
            {
                ImGui::SetNextWindowSize(ImVec2(600.f, 400.f));
                IGFD::FileDialogConfig const config =
                {
                    .path = ".",
                    .flags = ImGuiFileDialogFlags_DontShowHiddenFiles
                           | ImGuiFileDialogFlags_DisableCreateDirectoryButton
                           | ImGuiFileDialogFlags_HideColumnType
                           | ImGuiFileDialogFlags_ReadOnlyFileNameField
                           | ImGuiFileDialogFlags_Modal,
                };
                ImGuiFileDialog::Instance()->OpenDialog
                (
                    "ChooseFile",
                    "Выберите файл",
                    ".cpp, .h",
                    config
                );
            }

            if(ImGuiFileDialog::Instance()->Display("ChooseFile"))
            {
                if(ImGuiFileDialog::Instance()->IsOk())
                    path = ImGuiFileDialog::Instance()->GetFilePathName();
                ImGuiFileDialog::Instance()->Close();
            }
            ImGui::Text("Последний выбранный путь: %s", path.c_str());

            ImGui::End();
        }
        
    }
};

int main()
{
    vk::ext::guard::GLFW glfw({});
    auto const &[glfwVersion, glfwVersionString, glfwExt] = glfw.cref();
    std::cout << glfwVersionString << std::endl;

    auto const
    [
        instance,
        physicalDevice,
        deviceInfo,
        device,
        genericQueueFamily,
        computeQueueFamily,
        graphicsQueueFamily,
        queueFamilyRelations,
        genericQueue,
        computeQueue,
        graphicsQueue
    ] = vk::ext::trivial::Device
    ({
        .instance = vk::ext::trivial::Instance
        ({
            .apiVersion = VK_API_VERSION_1_1,
            //.validationLayer = {{"VK_LAYER_KHRONOS_validation"}},
            .extension = glfwExt,
        }),
        .requiredExtensions = {{"VK_KHR_swapchain"}},
    });

    vk::ext::GLFWSurface surface(instance);
    auto const fmt = surface.availableFormat(physicalDevice);
    auto const capabilities = surface.capabilities(physicalDevice);

    vk::ext::imgui::RenderSurface renderSurface
    ({
        .instance = instance,
        .physicalDevice = physicalDevice,
        .device = device,
        .queueFamily = graphicsQueueFamily,
        .queue = graphicsQueue,
        .window = surface.window,
        .surface = surface.surface,
        .surfaceFormat = static_cast<int>(fmt[0].format) % 7 == 1 ? fmt[1] : fmt[0],
        .minImageCount = capabilities.maxImageCount > 2u ? 3u : capabilities.maxImageCount,
    });

    ImFontConfig fontConfig;
    std::vector<char> fontBuffer;
    {
        std::ifstream in(IMGUI_RES "/Roboto-Medium.ttf", std::ios::binary);
        if(!in)
            throw std::runtime_error("incorrect font path");

        fontBuffer = std::vector<char>
        {
            std::istreambuf_iterator<char>(in),
            std::istreambuf_iterator<char>(),
        };

        fontConfig.FontData = fontBuffer.data();
        fontConfig.FontDataSize = static_cast<int>(fontBuffer.size());
        fontConfig.FontDataOwnedByAtlas = false;
        fontConfig.SizePixels = 16.f;
        fontConfig.GlyphRanges = ImFontAtlas{}.GetGlyphRangesCyrillic();
        std::strcpy(fontConfig.Name, "Roboto-Medium, 16pt");
    };

    GUI gui = {renderSurface.imguiContextGuard({{fontConfig}})};
    ImGui_ImplGlfw_InstallCallbacks(surface.window);

    while(!glfwWindowShouldClose(surface.window))
    {
        glfwPollEvents();

        auto const frame = renderSurface.frameGuard();
        auto const cmd = vk::guard::CommandBuffer
        ({
            .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
            .commandBuffer = frame.cref().commandBuffer,
        });
        auto const renderPass = renderSurface.renderPassGuard(frame);

        gui.render(cmd);
    }
    vkDeviceWaitIdle(device);
}
